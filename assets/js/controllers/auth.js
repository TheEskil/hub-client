'use strict';

angular.module('app')
    .controller('AuthController', AuthController);

function AuthController($auth, $state, $rootScope, hub) {
    var vm = this;

    vm.loading = false;

    vm.login = function () {
        vm.loading = true;
        vm.loginError = false;
        vm.loginErrorText = null;

        var credentials = {
            email: vm.email,
            password: vm.password
        }

        $auth.login(credentials)
            .then(function () {
                return hub.getAuthenticatedUser();
            }, function (error) {
                vm.loading = false;
                vm.loginError = true;
                vm.loginErrorText = error.data.error.message;
            })
            .then(function (response) {
                if (response) {
                    var user = JSON.stringify(response.data.user);

                    localStorage.setItem('user', user);
                    $rootScope.authenticated = true;
                    $rootScope.currentUser = response.data.user;

                    $state.go('app.dashboard');
                }
            });
    }

    vm.logout = function () {
        $rootScope.authenticated = false;

        localStorage.removeItem('user');
        localStorage.removeItem('satellizer_token');

        $state.go('login');
    }
}