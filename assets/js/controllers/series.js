'use strict';

angular.module('app')
    .controller('SeriesController', ['$scope', 'hub', '$uibModal', '$log', function ($scope, hub, $uibModal, $log) {
        $scope.loading = true;
        $scope.listOfSeries = [];

        var seriesPoller = hub.getAllSeries();
        seriesPoller.promise.then(null, null, function (result) {
            if(result.status === 200) {
                $scope.listOfSeries = result.data.data;
            }
            else {
                $log.error(result);
            }

            $scope.loading = false;
        });

        $scope.$on("$destroy", function () {
            seriesPoller.stop();
        });

        $scope.openAddAliasModal = function (series) {
            $scope.series = series;

            var modalInstance = $uibModal.open({
                animation: false,
                templateUrl: 'tpl/modals/add_alias.html',
                controller: 'AliasModalController',
                resolve: {
                    series: function () {
                        return $scope.series;
                    },
                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load([
                                'assets/plugins/angular-ui-bootstrap/angular-ui-bootstrap.js'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            })
                            .then(function () {
                                return $ocLazyLoad.load([
                                    'assets/js/controllers/series.js'
                                ]);
                            });
                    }]
                }
            });

            modalInstance.result.then(function () {
            }, function () {
            });
        };

        $scope.refresh = function (series) {
            console.log('Refresh series: ' + series);

            hub.refreshSeries(series.id).then(
                function (success) {
                    $('body').pgNotification({
                        style: 'bar',
                        message: success.data.data.message,
                        position: 'top',
                        timeout: 3000,
                        type: 'success'
                    }).show();
                },
                function (error) {
                    $('body').pgNotification({
                        style: 'bar',
                        message: error.data.error.message,
                        position: 'top',
                        timeout: 3000,
                        type: 'error'
                    }).show();
                }
            );
        };

        $scope.refreshAll = function () {
            hub.refreshAllSeries().then(
                function (success) {
                    $('body').pgNotification({
                        style: 'bar',
                        message: success.data.data.message,
                        position: 'top',
                        timeout: 3000,
                        type: 'success'
                    }).show();
                },
                function (error) {
                    $('body').pgNotification({
                        style: 'bar',
                        message: error.data.error.message,
                        position: 'top',
                        timeout: 3000,
                        type: 'error'
                    }).show();
                }
            );
        };

        $scope.rebuildFolders = function () {
            hub.rebuildFolders().then(
                function (success) {
                    $('body').pgNotification({
                        style: 'bar',
                        message: success.data.data.message,
                        position: 'top',
                        timeout: 3000,
                        type: 'success'
                    }).show();
                },
                function (error) {
                    $('body').pgNotification({
                        style: 'bar',
                        message: error.data.error.message,
                        position: 'top',
                        timeout: 3000,
                        type: 'error'
                    }).show();
                }
            );
        };

        $scope.deleteAlias = function (alias) {
            $log.info('Deleting alias ' + alias.title + ' with id of ' + alias.id);

            outerLoop:
                for (var i = 0; i < $scope.listOfSeries.length; i++) {
                    for (var o = 0; o < $scope.listOfSeries[i].aliases.length; o++) {
                        if ($scope.listOfSeries[i].aliases[o].id === alias.id) {
                            hub.deleteAlias(alias.id).then(
                                function (success) {
                                    $('body').pgNotification({
                                        style: 'bar',
                                        message: success.data.data.message,
                                        position: 'top',
                                        timeout: 3000,
                                        type: 'success'
                                    }).show();

                                    $scope.listOfSeries[i].aliases.splice(o, 1);
                                },
                                function (error) {
                                    $('body').pgNotification({
                                        style: 'bar',
                                        message: error.data.error.message,
                                        position: 'top',
                                        timeout: 3000,
                                        type: 'error'
                                    }).show();
                                }
                            );
                            break outerLoop;
                        }
                    }
                }
        };

        $scope.openDeleteSeriesModal = function ($event, series) {
            $scope.series = series;

            if ($event.shiftKey) {
                $log.info('Shift key was down');
            }
            else {
                var modalInstance = $uibModal.open({
                    animation: false,
                    templateUrl: 'tpl/modals/delete_series.html',
                    controller: 'SeriesModalController',
                    resolve: {
                        series: function () {
                            return $scope.series;
                        },
                        listOfSeries: function () {
                            return $scope.listOfSeries;
                        },
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                    'assets/plugins/angular-ui-bootstrap/angular-ui-bootstrap.js'
                                ], {
                                    insertBefore: '#lazyload_placeholder'
                                })
                                .then(function () {
                                    return $ocLazyLoad.load([
                                        'assets/js/controllers/series.js'
                                    ]);
                                });
                        }]
                    }
                });

                modalInstance.result.then(function () {
                }, function () {
                });
            }
        };
    }]);

angular.module('app')
    .controller('AliasModalController', function ($scope, $http, $uibModalInstance, $log, series, hub) {
        $scope.series = series;

        $scope.formData = {
            keepWindowOpen: false,
        };

        $scope.add = function () {
            $scope.message = '';
            $scope.errorTitle = '';

            hub.addAlias({
                'title': $scope.formData.title,
                'series_id': series.id
            }).then(
                function (success) {
                    $scope.series.aliases.push(success.data.data.created_record);

                    if ($scope.formData.keepWindowOpen == true) {
                        $scope.formData.title = '';
                        $scope.message = success.data.data.message;
                    }
                    else {
                        $('body').pgNotification({
                            style: 'bar',
                            message: success.data.data.message,
                            position: 'top',
                            timeout: 3000,
                            type: 'success'
                        }).show();

                        $uibModalInstance.close($scope.series.aliases);
                    }
                },
                function (error) {
                    $scope.errorTitle = error.data.error.message.title[0];
                }
            );
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    });

angular.module('app')
    .controller('SeriesModalController', function ($scope, hub, $uibModalInstance, $log, series, listOfSeries) {
        $scope.series = series;
        $scope.listOfSeries = listOfSeries;

        $scope.delete = function () {
            hub.deleteSeries(series.id).then(
                function (success) {
                    for (var i = 0; i < $scope.listOfSeries.length; i++) {
                        if ($scope.listOfSeries[i].id == $scope.series.id) {
                            $scope.listOfSeries.splice(i, 1);

                            $('body').pgNotification({
                                style: 'bar',
                                message: success.data.data.message + ': ' + success.data.data.deleted_record.title,
                                position: 'top',
                                timeout: 3000,
                                type: 'success'
                            }).show();
                            break;
                        }
                    }

                    $uibModalInstance.close($scope.series.aliases);
                },
                function (error) {
                    $('body').pgNotification({
                        style: 'bar',
                        message: error.data.error.message,
                        position: 'top',
                        timeout: 3000,
                        type: 'error'
                    }).show();
                }
            );
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    });