'use strict';

angular.module('app')
    .controller('SearchResultsController', ['$scope', 'hub', '$state', '$stateParams', function ($scope, hub, $state, $stateParams) {
        $scope.searchForSeries = true;
        $scope.searchForTorrents = true;

        $scope.searchResults = {
            series: [],
            torrents: [],
        };

        if ($state.current.name == "app.search" && $stateParams.query) {
            $scope.searchQuery = $stateParams.query;

            if ($scope.searchForSeries) {
                $scope.seriesLoading = true;
                hub.searchSeries($stateParams.query).success(function (data, status, headers, config) {
                    $scope.seriesLoading = false;
                    $scope.searchResults.series = data.data;
                }).error(function (data, status, headers, config) {
                    $scope.seriesLoading = false;
                    $scope.searchResults.series = [];
                });
            }
            if ($scope.searchForTorrents) {
                $scope.torrentsLoading = true;
                hub.searchTorrents($stateParams.query).success(function (data, status, headers, config) {
                    $scope.torrentsLoading = false;
                    $scope.searchResults.torrents = data.data;
                }).error(function (data, status, headers, config) {
                    $scope.torrentsLoading = false;
                    $scope.searchResults.torrents = [];
                });
            }
        }

        $scope.addSeries = function (series) {
            hub.addSeries(series.tvmaze_id).then(
                function (success) {
                    console.log(success);
                    $('body').pgNotification({
                        style: 'bar',
                        message: success.data.data.message + ' "' + success.data.data.created_record.title + '"',
                        position: 'top',
                        timeout: 3000,
                        type: 'success'
                    }).show();
                },
                function (error) {
                    console.log(error);
                    $('body').pgNotification({
                        style: 'bar',
                        message: error.data.error.message.tvmaze_id[0],
                        position: 'top',
                        timeout: 3000,
                        type: 'error'
                    }).show();
                }
            );
        }

        $scope.downloadTorrent = function (torrent) {
            hub.downloadTorrent(torrent.id).then(
                function (success) {
                    $('body').pgNotification({
                        style: 'bar',
                        message: success.data.data.message + ' "' + success.data.data.downloaded_record.title + '"',
                        position: 'top',
                        timeout: 3000,
                        type: 'success'
                    }).show();
                },
                function (error) {
                    $('body').pgNotification({
                        style: 'bar',
                        message: error.data.error.message,
                        position: 'top',
                        timeout: 3000,
                        type: 'error'
                    }).show();
                }
            );
        }
    }]);