'use strict';

angular.module('app')
    .controller('SeriesDetailsController', ['$scope', 'hub', '$stateParams', '$uibModal', '$log', function ($scope, hub, $stateParams, $uibModal, $log) {
        $scope.seriesLoading = true;
        $scope.episodesLoading = true;

        var seriesPoller = hub.getSeries($stateParams.seriesId);
        seriesPoller.promise.then(null, null, function (result) {
            if (result.status === 200) {
                $scope.series = result.data.data;
            }
            else {
                $log.error(result);
            }

            $scope.seriesLoading = false;
        });

        var seriesEpisodesPoller = hub.getSeriesEpisodes($stateParams.seriesId);
        seriesEpisodesPoller.promise.then(null, null, function (result) {
            if (result.status === 200) {
                $scope.episodes = result.data.data;
            }
            else {
                $log.error(result);
            }

            $scope.episodesLoading = false;
        });

        $scope.$on("$destroy", function () {
            seriesPoller.stop();
            seriesEpisodesPoller.stop();
        });

        $scope.showTorrentsModal = function (episode) {
            $scope.episode = episode;

            var modalInstance = $uibModal.open({
                animation: false,
                templateUrl: 'tpl/modals/episode_torrents.html',
                controller: 'TorrentsModalController',
                resolve: {
                    episode: function () {
                        return $scope.episode;
                    },
                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load([
                                'assets/plugins/angular-ui-bootstrap/angular-ui-bootstrap.js'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            })
                            .then(function () {
                                return $ocLazyLoad.load([
                                    'assets/js/controllers/dashboard.js'
                                ]);
                            });
                    }]
                }
            });

            modalInstance.result.then(function () {
            }, function () {
            });
        };

        $scope.refreshSeries = function (series) {
            alert('Not implemented yet');
        };

        $scope.deleteSeries = function (series) {
            alert('Not implemented yet');
        };

        $scope.searchEpisode = function (episode) {
            alert('Not implemented yet');
        };

        $scope.deleteEpisode = function (episode) {
            alert('Not implemented yet');
        };
    }]);