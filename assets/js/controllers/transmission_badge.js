'use strict';

angular.module('app')
    .controller('TransmissionBadgeController', ['$scope', 'hub', '$log', function ($scope, hub, $log) {
        $scope.transmission = {
            finished: 0,
            unfinished: 0,
            progressPercentage: 0,
        };

        var transmissionBadgePoller = hub.getTransmissionBadge(true);
        transmissionBadgePoller.promise.then(null, null, function (result) {
            if (result.status == 200 && result.data.data != undefined) {
                $scope.transmission = {
                    finished: result.data.data.finished,
                    unfinished: result.data.data.unfinished,
                    progressPercentage: result.data.data.progressPercentage,
                };
            }
            else {
                $scope.transmission = {
                    finished: "!"
                }
            }
        });
    }]);