'use strict';

angular.module('app')
    .controller('FilesController', ['$scope', 'hub', '$log', '$stateParams', function ($scope, hub, $log, $stateParams) {
        $scope.loading = true;
        $scope.files = [];

        var filesPoller = hub.getAllFiles($stateParams.page);
        filesPoller.promise.then(null, null, function (result) {
            if (result.status === 200) {
                $scope.files = result.data;
            }
            else {
                $log.error(result);
            }

            $scope.loading = false;
        });

        $scope.$on("$destroy", function () {
            filesPoller.stop();
        });

        $scope.deleteFile = function (file) {
            hub.deleteFile(file).then(function (success) {
                    for (var i = 0; i < $scope.files.data.length; i++) {
                        if ($scope.files.data[i].id == file.id) {
                            $scope.files.data.splice(i, 1);

                            $('body').pgNotification({
                                style: 'bar',
                                message: success.data.data.message + ': ' + success.data.data.deleted_record.file,
                                position: 'top',
                                timeout: 3000,
                                type: 'success'
                            }).show();
                            break;
                        }
                    }
                },
                function (error) {
                    $('body').pgNotification({
                        style: 'bar',
                        message: error.data.error.message,
                        position: 'top',
                        timeout: 3000,
                        type: 'error'
                    }).show();
                });
        }
    }]);