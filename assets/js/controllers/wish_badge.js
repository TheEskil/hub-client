'use strict';

angular.module('app')
    .controller('WishBadgeController', ['$scope', 'hub', '$log', function ($scope, hub, $log) {
        $scope.wishes = {
            fulfilled: 0,
            unfulfilled: 0,
        };

        var wishBadgePoller = hub.getWishBadge(true);
        wishBadgePoller.promise.then(null, null, function (result) {
            if (result.status == 200) {
                $scope.wishes = {
                    fulfilled: result.data.data.fulfilled,
                    unfulfilled: result.data.data.unfulfilled,
                };
            }
            else {
                $log.error(result);
            }
        });
    }]);