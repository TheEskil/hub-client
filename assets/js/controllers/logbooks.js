'use strict';

angular.module('app')
    .controller('LogbooksController', ['$scope', 'hub', '$log', function ($scope, hub, $log) {
        $scope.loading = true;
        $scope.logbooks = [];

        var logbooksPoller = hub.getAllLogbooks();
        logbooksPoller.promise.then(null, null, function (result) {
            if (result.status === 200) {
                $scope.logbooks = result.data.data;
            }
            else {
                $log.error(result);
            }

            $scope.loading = false;
        });

        $scope.$on("$destroy", function () {
            logbooksPoller.stop();
        });
    }]);
