'use strict';

angular.module('app')
    .controller('SearchController', ['$scope', 'hub', '$state', function ($scope, hub, $state) {
        $scope.searchForSeries = true;
        $scope.searchForTorrents = true;

        $scope.search = function () {
            $state.go('app.search', {query: $scope.search.query});
        };
    }]);