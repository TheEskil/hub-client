'use strict';

angular.module('app')
    .controller('FeedDetailsController', ['$scope', 'hub', '$stateParams', '$log', function ($scope, hub, $stateParams, $log) {
        $scope.feedLoading = true;

        var feedPoller = hub.getFeed($stateParams.feedId, $stateParams.page);
        feedPoller.promise.then(null, null, function (result) {
            if (result.status === 200) {
                $scope.feed = result.data.data.feed;
                $scope.torrents = result.data.data.torrents;
            }
            else {
                $log.error(result);
            }

            $scope.feedLoading = false;
        });

        $scope.$on("$destroy", function () {
            feedPoller.stop();
        });

        $scope.downloadTorrent = function (torrent) {
            hub.downloadTorrent(torrent.id).then(
                function (success) {
                    $('body').pgNotification({
                        style: 'bar',
                        message: success.data.data.message + ' "' + success.data.data.downloaded_record.title + '"',
                        position: 'top',
                        timeout: 3000,
                        type: 'success'
                    }).show();
                },
                function (error) {
                    $('body').pgNotification({
                        style: 'bar',
                        message: error.data.error.message,
                        position: 'top',
                        timeout: 3000,
                        type: 'error'
                    }).show();
                }
            );
        };
    }]);