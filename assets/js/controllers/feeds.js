'use strict';

angular.module('app')
    .controller('FeedsController', ['$scope', 'hub', '$uibModal', '$log', function ($scope, hub, $uibModal, $log) {
        $scope.loading = true;
        $scope.feeds = [];

        var feedsPoller = hub.getAllFeeds();
        feedsPoller.promise.then(null, null, function (result) {
            if (result.status === 200) {
                $scope.feeds = result.data.data;
            }
            else {
                $log.error(result);
            }

            $scope.loading = false;
        });

        $scope.$on("$destroy", function () {
            feedsPoller.stop();
        });

        $scope.refreshAll = function () {
            hub.refreshAllFeeds().then(
                function (success) {
                    $('body').pgNotification({
                        style: 'bar',
                        message: success.data.data.message,
                        position: 'top',
                        timeout: 3000,
                        type: 'success'
                    }).show();
                },
                function (error) {
                    $('body').pgNotification({
                        style: 'bar',
                        message: error.data.error.message,
                        position: 'top',
                        timeout: 3000,
                        type: 'error'
                    }).show();
                }
            );
        };

        $scope.openAddFeedModal = function () {
            var modalInstance = $uibModal.open({
                animation: false,
                templateUrl: 'tpl/modals/add_feed.html',
                controller: 'FeedModalController',
                resolve: {
                    feed: function () {
                        return null
                    },
                    feeds: function () {
                        return $scope.feeds;
                    },
                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load([
                                'assets/plugins/angular-ui-bootstrap/angular-ui-bootstrap.js'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            })
                            .then(function () {
                                return $ocLazyLoad.load([
                                    'assets/js/controllers/folders.js'
                                ]);
                            });
                    }]
                }
            });

            modalInstance.result.then(function () {
            }, function () {
            });
        };

        $scope.openDeleteFeedModal = function ($event, feed) {
            $scope.feed = feed;

            $log.info(feed);

            if ($event.shiftKey) {
                $log.info('Shift key was down');
            }
            else {
                var modalInstance = $uibModal.open({
                    animation: false,
                    templateUrl: 'tpl/modals/delete_feed.html',
                    controller: 'FeedModalController',
                    resolve: {
                        feed: function () {
                            return $scope.feed;
                        },
                        feeds: function () {
                            return $scope.feeds;
                        },
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                    'assets/plugins/angular-ui-bootstrap/angular-ui-bootstrap.js'
                                ], {
                                    insertBefore: '#lazyload_placeholder'
                                })
                                .then(function () {
                                    return $ocLazyLoad.load([
                                        'assets/js/controllers/feeds.js'
                                    ]);
                                });
                        }]
                    }
                });

                modalInstance.result.then(function () {
                }, function () {
                });
            }
        };
    }]);

angular.module('app')
    .controller('FeedModalController', function ($scope, hub, $uibModalInstance, $log, feed, feeds) {
        $scope.feed = feed;
        $scope.feeds = feeds;

        $scope.formData = {
            keepWindowOpen: false,
        };

        $scope.add = function () {
            $scope.message = '';
            $scope.errorTitle = '';
            $scope.errorUri = '';

            hub.addFeed({
                'title': $scope.formData.title,
                'uri': $scope.formData.uri,
            }).then(
                function (success) {
                    $scope.feeds.push(success.data.data.created_record);

                    if ($scope.formData.keepWindowOpen == true) {
                        $scope.formData.title = '';
                        $scope.formData.uri = '';
                        $scope.message = success.data.data.message;
                    }
                    else {
                        $('body').pgNotification({
                            style: 'bar',
                            message: success.data.data.message,
                            position: 'top',
                            timeout: 3000,
                            type: 'success'
                        }).show();

                        $uibModalInstance.close($scope.feeds);
                    }
                },
                function (error) {
                    if (error.data.error.message.title) {
                        $scope.errorTitle = error.data.error.message.title[0];
                    }
                    if (error.data.error.message.uri) {
                        $scope.errorUri = error.data.error.message.uri[0];
                    }
                }
            );
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

        $scope.delete = function () {
            hub.deleteFeed(feed.id).then(
                function (success) {
                    for (var i = 0; i < $scope.feeds.length; i++) {
                        if ($scope.feeds[i].id == $scope.feed.id) {
                            $scope.feeds.splice(i, 1);

                            $('body').pgNotification({
                                style: 'bar',
                                message: success.data.data.message,
                                position: 'top',
                                timeout: 3000,
                                type: 'success'
                            }).show();
                            break;
                        }
                    }

                    $uibModalInstance.close($scope.feeds);
                },
                function (error) {
                    $('body').pgNotification({
                        style: 'bar',
                        message: error.data.error.message,
                        position: 'top',
                        timeout: 3000,
                        type: 'error'
                    }).show();
                }
            );
        };
    });