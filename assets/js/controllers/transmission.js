'use strict';

angular.module('app')
    .controller('TransmissionController', ['$scope', 'hub', '$log', function ($scope, hub, $log) {
        $scope.loading = true;
        $scope.torrents = [];

        var transmissionPoller = hub.getTransmissionTorrents();
        transmissionPoller.promise.then(null, null, function (result) {
            if (result.status === 200) {
                $scope.torrents = result.data.data;
            }
            else {
                $log.error(result);
            }

            $scope.loading = false;
        });

        $scope.$on("$destroy", function () {
            transmissionPoller.stop();
        });

        $scope.startAll = function () {
            hub.startAllTransmissionTorrents().then(
                function (success) {
                    if (success.data.error) {
                        $('body').pgNotification({
                            style: 'bar',
                            message: success.data.error.message,
                            position: 'top',
                            timeout: 3000,
                            type: 'error'
                        }).show();
                    }
                    else {
                        $('body').pgNotification({
                            style: 'bar',
                            message: success.data,
                            position: 'top',
                            timeout: 3000,
                            type: 'success'
                        }).show();
                    }
                },
                function (error) {
                    $log.error(error);
                }
            );
        };

        $scope.stopAll = function () {
            hub.stopAllTransmissionTorrents().then(
                function (success) {
                    if (success.data.error) {
                        $('body').pgNotification({
                            style: 'bar',
                            message: success.data.error.message,
                            position: 'top',
                            timeout: 3000,
                            type: 'error'
                        }).show();
                    }
                    else {
                        $('body').pgNotification({
                            style: 'bar',
                            message: success.data,
                            position: 'top',
                            timeout: 3000,
                            type: 'success'
                        }).show();
                    }
                },
                function (error) {
                    $log.error(error);
                }
            );
        };

        $scope.removeAllFinished = function () {
            hub.removeAllFinishedTransmissionTorrents().then(
                function (success) {
                    if (success.data.error) {
                        $('body').pgNotification({
                            style: 'bar',
                            message: success.data.error.message,
                            position: 'top',
                            timeout: 3000,
                            type: 'error'
                        }).show();
                    }
                    else {
                        $('body').pgNotification({
                            style: 'bar',
                            message: success.data,
                            position: 'top',
                            timeout: 3000,
                            type: 'success'
                        }).show();
                    }
                },
                function (error) {
                    $log.error(error);
                }
            );
        };

        $scope.stop = function (torrent) {
            hub.stopTransmissionTorrent(torrent.hash).then(
                function (success) {
                    if (success.data.error) {
                        $('body').pgNotification({
                            style: 'bar',
                            message: success.data.error.message,
                            position: 'top',
                            timeout: 3000,
                            type: 'error'
                        }).show();
                    }
                    else {
                        $('body').pgNotification({
                            style: 'bar',
                            message: success.data,
                            position: 'top',
                            timeout: 3000,
                            type: 'success'
                        }).show();
                    }
                },
                function (error) {
                    $log.error(error);
                }
            );
        };

        $scope.start = function (torrent) {
            hub.startTransmissionTorrent(torrent.hash).then(
                function (success) {
                    if (success.data.error) {
                        $('body').pgNotification({
                            style: 'bar',
                            message: success.data.error.message,
                            position: 'top',
                            timeout: 3000,
                            type: 'error'
                        }).show();
                    }
                    else {
                        $('body').pgNotification({
                            style: 'bar',
                            message: success.data,
                            position: 'top',
                            timeout: 3000,
                            type: 'success'
                        }).show();
                    }
                },
                function (error) {
                    $log.error(error);
                }
            );
        };

        $scope.remove = function (torrent, removeLocalData) {
            hub.removeTransmissionTorrent(torrent.hash, removeLocalData).then(
                function (success) {
                    if (success.data.error) {
                        $('body').pgNotification({
                            style: 'bar',
                            message: success.data.error.message,
                            position: 'top',
                            timeout: 3000,
                            type: 'error'
                        }).show();
                    }
                    else {
                        $('body').pgNotification({
                            style: 'bar',
                            message: success.data,
                            position: 'top',
                            timeout: 3000,
                            type: 'success'
                        }).show();
                    }
                },
                function (error) {
                    $log.error(error);
                }
            );
        };
    }]);