'use strict';

angular.module('app')
    .controller('FilesDownloadsController', ['$scope', 'hub', '$log', function ($scope, hub, $log) {
        $scope.loading = true;
        $scope.files = [];

        var filesDownloadsPoller = hub.getAllFilesDownloads();
        filesDownloadsPoller.promise.then(null, null, function (result) {
            if (result.status === 200) {
                $scope.files = result.data.data;
            }
            else {
                $log.error(result);
            }

            $scope.loading = false;
        });

        $scope.$on("$destroy", function () {
            filesDownloadsPoller.stop();
        });

        $scope.processAllFiles = function () {
            hub.processFilesDownloads().then(function (success) {
                    $('body').pgNotification({
                        style: 'bar',
                        message: success.data.data.message,
                        position: 'top',
                        timeout: 3000,
                        type: 'success'
                    }).show();
                },
                function (error) {
                    $('body').pgNotification({
                        style: 'bar',
                        message: error.data.error.message,
                        position: 'top',
                        timeout: 3000,
                        type: 'error'
                    }).show();
                });
        };
    }]);