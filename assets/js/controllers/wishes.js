'use strict';

angular.module('app')
    .controller('WishesController', ['$scope', 'hub', '$log', '$uibModal', function ($scope, hub, $log, $uibModal) {
        $scope.loading = true;
        $scope.wishes = [];

        var wishesPoller = hub.getAllWishes();
        wishesPoller.promise.then(null, null, function (result) {
            if (result.status === 200) {
                $scope.wishes = result.data.data;
            }
            else {
                $log.error(result);
            }

            $scope.loading = false;
        });

        $scope.$on("$destroy", function () {
            wishesPoller.stop();
        });

        $scope.refreshWishes = function () {
            hub.refreshAllWishes().then(
                function (success) {
                    $('body').pgNotification({
                        style: 'bar',
                        message: success.data.data.message,
                        position: 'top',
                        timeout: 3000,
                        type: 'success'
                    }).show();
                },
                function (error) {
                    $('body').pgNotification({
                        style: 'bar',
                        message: error.data.error.message,
                        position: 'top',
                        timeout: 3000,
                        type: 'error'
                    }).show();
                }
            );
        };

        $scope.refreshWish = function (wish) {
            hub.refreshWish(wish.id).then(
                function (success) {
                    $('body').pgNotification({
                        style: 'bar',
                        message: success.data.data.message,
                        position: 'top',
                        timeout: 3000,
                        type: 'success'
                    }).show();
                },
                function (error) {
                    $('body').pgNotification({
                        style: 'bar',
                        message: error.data.error.message,
                        position: 'top',
                        timeout: 3000,
                        type: 'error'
                    }).show();
                }
            );
        };

        $scope.openDeleteWishModal = function ($event, wish) {
            $scope.wish = wish;

            if ($event.shiftKey) {
                $log.info('Shift key was down');
            }
            else {
                var modalInstance = $uibModal.open({
                    animation: false,
                    templateUrl: 'tpl/modals/delete_wish.html',
                    controller: 'WishModalController',
                    resolve: {
                        wish: function () {
                            return $scope.wish;
                        },
                        listOfWishes: function () {
                            return $scope.wishes;
                        },
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                    'assets/plugins/angular-ui-bootstrap/angular-ui-bootstrap.js'
                                ], {
                                    insertBefore: '#lazyload_placeholder'
                                })
                                .then(function () {
                                    return $ocLazyLoad.load([
                                        'assets/js/controllers/wishes.js'
                                    ]);
                                });
                        }]
                    }
                });

                modalInstance.result.then(function () {
                }, function () {
                });
            }
        }
    }]);

angular.module('app')
    .controller('WishModalController', function ($scope, hub, $uibModalInstance, $log, wish, listOfWishes) {
        $scope.wish = wish;
        $scope.listOfWishes = listOfWishes;

        $scope.delete = function () {
            hub.deleteWish(wish.id).then(
                function (success) {
                    for (var i = 0; i < $scope.listOfWishes.length; i++) {
                        if ($scope.listOfWishes[i].id == $scope.wish.id) {
                            $scope.listOfWishes.splice(i, 1);

                            $('body').pgNotification({
                                style: 'bar',
                                message: success.data.data.message + ' "' + success.data.data.deleted_record.title + '"',
                                position: 'top',
                                timeout: 3000,
                                type: 'success'
                            }).show();
                            break;
                        }
                    }

                    $uibModalInstance.close();
                },
                function (error) {
                    $('body').pgNotification({
                        style: 'bar',
                        message: error.data.error.message,
                        position: 'top',
                        timeout: 3000,
                        type: 'error'
                    }).show();
                }
            );
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    });