'use strict';

angular.module('app')
    .controller('DashboardController', ['$scope', 'hub', '$log', '$uibModal', function ($scope, hub, $log, $uibModal) {
        $scope.episodes = {
            recent: {
                data: [],
                loading: true,
            },
            upcoming: {
                data: [],
                loading: true,
            },
        };

        var recentEpisodesPoller = hub.getRecentEpisodes();
        recentEpisodesPoller.promise.then(null, null, function (result) {
            if (result.status === 200) {
                $scope.episodes.recent.data = result.data.data;
            }
            else {
                $log.error(result);
            }

            $scope.episodes.recent.loading = false;
        });

        var upcomingEpisodesPoller = hub.getUpcomingEpisodes();
        upcomingEpisodesPoller.promise.then(null, null, function (result) {
            if (result.status === 200) {
                $scope.episodes.upcoming.data = result.data.data;
            }
            else {
                $log.error(result);
            }

            $scope.episodes.upcoming.loading = false;
        });

        $scope.$on("$destroy", function () {
            recentEpisodesPoller.stop();
            upcomingEpisodesPoller.stop();
        });

        $scope.showTorrentsModal = function (episode) {
            $scope.episode = episode;

            var modalInstance = $uibModal.open({
                animation: false,
                templateUrl: 'tpl/modals/episode_torrents.html',
                controller: 'TorrentsModalController',
                resolve: {
                    episode: function () {
                        return $scope.episode;
                    },
                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load([
                                'assets/plugins/angular-ui-bootstrap/angular-ui-bootstrap.js'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            })
                            .then(function () {
                                return $ocLazyLoad.load([
                                    'assets/js/controllers/dashboard.js'
                                ]);
                            });
                    }]
                }
            });

            modalInstance.result.then(function () {
            }, function () {
            });
        }
    }]);