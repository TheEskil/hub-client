angular.module('app')
    .factory('hub', ['$http', 'HUB_CONFIG', 'poller', function ($http, HUB_CONFIG, poller) {
        var hub = {};

        hub.authenticate = function (email, password) {
            return $http.post(HUB_CONFIG.AUTH_ENDPOINT, {'email': email, 'password': password})
        };

        hub.getAuthenticatedUser = function () {
            return $http.get(HUB_CONFIG.BASE_URL + '/auth/user');
        };

        hub.searchSeries = function (query) {
            return $http.get(HUB_CONFIG.API_URL + '/tvmaze/fullsearch/' + query);
        };

        hub.addSeries = function (tvMazeId) {
            return $http.post(HUB_CONFIG.API_URL + '/series', {tvmaze_id: tvMazeId});
        };

        hub.getWishBadge = function () {
            return poller.get(HUB_CONFIG.API_URL + '/wishes/badge', {
                delay: 10000,
                catchError: true,
            });
        };

        hub.getTransmissionBadge = function () {
            return poller.get(HUB_CONFIG.API_URL + '/transmission/badge', {
                delay: 10000,
                catchError: true,
            });
        };

        hub.searchTorrents = function (query) {
            return $http.get(HUB_CONFIG.API_URL + '/torrents/search/' + query);
        };

        hub.getRecentEpisodes = function () {
            return poller.get(HUB_CONFIG.API_URL + '/episodes/recent/3', {
                delay: 10000,
                catchError: true,
            });
        };

        hub.getUpcomingEpisodes = function () {
            return poller.get(HUB_CONFIG.API_URL + '/episodes/upcoming', {
                delay: 10000,
                catchError: true,
            });
        };

        hub.getAllFeeds = function () {
            return poller.get(HUB_CONFIG.API_URL + '/feeds', {
                delay: 10000,
                catchError: true,
            });
        };

        hub.refreshAllFeeds = function () {
            return $http.get(HUB_CONFIG.API_URL + '/feeds/refresh');
        };

        hub.addFeed = function (feed) {
            return $http.post(HUB_CONFIG.API_URL + '/feeds', feed);
        };

        hub.deleteFeed = function (id) {
            return $http.delete(HUB_CONFIG.API_URL + '/feeds/' + id);
        };

        hub.getFeed = function (id, page) {
            if (typeof page === 'undefined') {
                return poller.get(HUB_CONFIG.API_URL + '/feeds/' + id, {
                    delay: 10000,
                    catchError: true,
                });
            }
            else {
                return poller.get(HUB_CONFIG.API_URL + '/feeds/' + id + '?page=' + page, {
                    delay: 10000,
                    catchError: true,
                });
            }
        };

        hub.getAllFiles = function (page) {
            if (typeof page === 'undefined') {
                return poller.get(HUB_CONFIG.API_URL + '/files', {
                    delay: 10000,
                    catchError: true,
                });
            }
            else {
                return poller.get(HUB_CONFIG.API_URL + '/files?page=' + page, {
                    delay: 10000,
                    catchError: true,
                });
            }
        };

        hub.deleteFile = function (file) {
            return $http.delete(HUB_CONFIG.API_URL + '/files/' + file.id);
        };

        hub.getAllFilesDownloads = function () {
            return poller.get(HUB_CONFIG.API_URL + '/files/downloads', {
                delay: 10000,
                catchError: true,
            });
        };

        hub.processFilesDownloads = function () {
            return $http.get(HUB_CONFIG.API_URL + '/files/process');
        };

        hub.getAllFolders = function () {
            return poller.get(HUB_CONFIG.API_URL + '/folders', {
                delay: 10000,
                catchError: true,
            });
        };

        hub.addFolder = function (folder) {
            return $http.post(HUB_CONFIG.API_URL + '/folders', folder);
        };

        hub.deleteFolder = function (id) {
            return $http.delete(HUB_CONFIG.API_URL + '/folders/' + id);
        };

        hub.getAllLogbooks = function () {
            return poller.get(HUB_CONFIG.API_URL + '/logbooks');
        };

        hub.getAllSeries = function () {
            return poller.get(HUB_CONFIG.API_URL + '/series', {
                delay: 10000,
                catchError: true,
            });
        };

        hub.refreshSeries = function (id) {
            return $http.get(HUB_CONFIG.API_URL + '/series/' + id + '/refresh', {
                delay: 10000,
                catchError: true,
            });
        };

        hub.refreshAllSeries = function () {
            return $http.get(HUB_CONFIG.API_URL + '/series/refresh');
        };

        hub.rebuildEpisodes = function () {
            return $http.get(HUB_CONFIG.API_URL + '/episodes/rebuild');
        };

        hub.rebuildFolders = function () {
            return $http.get(HUB_CONFIG.API_URL + '/folders/rebuild');
        };

        hub.deleteAlias = function (id) {
            return $http.delete(HUB_CONFIG.API_URL + '/aliases/' + id);
        };

        hub.addAlias = function (alias) {
            return $http.post(HUB_CONFIG.API_URL + '/aliases', alias);
        };

        hub.deleteSeries = function (id) {
            return $http.delete(HUB_CONFIG.API_URL + '/series/' + id);
        };

        hub.getSeries = function (id) {
            return poller.get(HUB_CONFIG.API_URL + '/series/' + id, {
                delay: 10000,
                catchError: true,
            });
        };

        hub.getSeriesEpisodes = function (id) {
            return poller.get(HUB_CONFIG.API_URL + '/series/' + id + '/episodes', {
                delay: 10000,
                catchError: true,
            });
        };

        hub.downloadTorrent = function (id, type, typeId) {
            if (typeof type !== undefined && typeof typeId !== undefined) {
                switch (type) {
                    case 'episode':
                        return $http.get(HUB_CONFIG.API_URL + '/torrents/' + id + '/download/episode/' + typeId);
                        break;
                    case 'wish':
                        return $http.get(HUB_CONFIG.API_URL + '/torrents/' + id + '/download/wish/' + typeId);
                        break;
                }
            }

            return $http.get(HUB_CONFIG.API_URL + '/torrents/' + id + '/download');
        };

        hub.getTransmissionTorrents = function () {
            return poller.get(HUB_CONFIG.API_URL + '/transmission', {
                delay: 10000,
                catchError: true,
            });
        };

        hub.startAllTransmissionTorrents = function () {
            return $http.get(HUB_CONFIG.API_URL + '/transmission/start/all');
        };

        hub.stopAllTransmissionTorrents = function () {
            return $http.get(HUB_CONFIG.API_URL + '/transmission/stop/all');
        };

        hub.removeAllFinishedTransmissionTorrents = function () {
            return $http.get(HUB_CONFIG.API_URL + '/transmission/remove/finished');
        };

        hub.stopTransmissionTorrent = function (hash) {
            return $http.get(HUB_CONFIG.API_URL + '/transmission/stop/' + hash);
        };

        hub.startTransmissionTorrent = function (hash) {
            return $http.get(HUB_CONFIG.API_URL + '/transmission/start/' + hash);
        };

        hub.removeTransmissionTorrent = function (hash, removeLocalData) {
            if (typeof removeLocalData === 'undefined') {
                return $http.get(HUB_CONFIG.API_URL + '/transmission/remove/' + hash);
            }
            else {
                return $http.get(HUB_CONFIG.API_URL + '/transmission/remove/' + hash + '/' + removeLocalData);
            }
        };

        hub.getAllWishes = function () {
            return poller.get(HUB_CONFIG.API_URL + '/wishes', {
                delay: 10000,
                catchError: true,
            });
        };

        hub.refreshAllWishes = function () {
            return $http.get(HUB_CONFIG.API_URL + '/wishes/refresh');
        };

        hub.refreshWish = function (id) {
            return $http.get(HUB_CONFIG.API_URL + '/wishes/' + id + '/refresh');
        };

        hub.deleteWish = function (id) {
            return $http.delete(HUB_CONFIG.API_URL + '/wishes/' + id);
        };

        return hub;
    }]);