angular.module('app')
    .filter('inArray', function () {
        return function (array, value) {
            for (var key in array) {
                if (array[key] == value) {
                    return true;
                }
            }

            return false;
        };
    });