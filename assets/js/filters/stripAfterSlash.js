angular.module('app')
    .filter('stripAfterSlash', function () {
        return function (item) {
            return item.replace(/\/.*/g, '');
        }
    });