/* ============================================================
 * File: main.js
 * Main Controller to set global scope variables. 
 * ============================================================ */

angular.module('app')
    .controller('AppCtrl', ['$scope', '$rootScope', '$state', function ($scope, $rootScope, $state) {

        // App globals
        $scope.app = {
            name: 'Hub4',
            description: '',
            layout: {
                menuPin: true,
                menuBehind: false,
                theme: 'pages/css/themes/retro.css'
            },
            author: 'blackmage'
        }

        // Checks if the given state is the current state
        $scope.is = function (name) {
            return $state.is(name);
        }

        // Checks if the given state/child states are present
        $scope.includes = function (name) {
            return $state.includes(name);
        }
    }]);