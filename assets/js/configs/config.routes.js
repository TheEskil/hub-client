/* ============================================================
 * File: config.js
 * Configure routing
 * ============================================================ */

angular.module('app')
    .config(['$stateProvider', '$urlRouterProvider', '$ocLazyLoadProvider',
        function ($stateProvider, $urlRouterProvider, $ocLazyLoadProvider) {
            $urlRouterProvider
                .otherwise('/app/dashboard');

            $stateProvider
                .state('app', {
                    abstract: true,
                    url: "/app",
                    templateUrl: "tpl/app.html"
                })
                .state('app.dashboard', {
                    url: "/dashboard",
                    templateUrl: "tpl/dashboard.html",
                    controller: 'DashboardController',
                    resolve: {
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                    'assets/js/services/hub.js',
                                    'assets/js/controllers/auth.js',
                                ], {
                                    insertBefore: '#lazyload_placeholder'
                                })
                                .then(function () {
                                    return $ocLazyLoad.load([
                                        'assets/js/controllers/dashboard.js',
                                        'assets/js/controllers/torrents_modal.js'
                                    ]);
                                });
                        }]
                    }
                })
                .state('app.series', {
                    url: "/series",
                    templateUrl: "tpl/series.html",
                    controller: 'SeriesController',
                    resolve: {
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                    'assets/js/services/hub.js',
                                    'assets/js/controllers/auth.js',
                                ], {
                                    insertBefore: '#lazyload_placeholder'
                                })
                                .then(function () {
                                    return $ocLazyLoad.load([
                                        'assets/js/controllers/series.js'
                                    ]);
                                });
                        }]
                    }
                })
                .state('app.series_details', {
                    url: "/series/:seriesId",
                    templateUrl: "tpl/series_details.html",
                    controller: 'SeriesDetailsController',
                    resolve: {
                        seriesId: ['$stateParams', function ($stateParams) {
                            return $stateParams.seriesId;
                        }],
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                    'assets/js/services/hub.js',
                                    'assets/js/controllers/auth.js',
                                    'assets/js/directives/ddTextCollapse.js',
                                ], {
                                    insertBefore: '#lazyload_placeholder'
                                })
                                .then(function () {
                                    return $ocLazyLoad.load([
                                        'assets/js/controllers/series_details.js',
                                        'assets/js/controllers/torrents_modal.js'
                                    ]);
                                });
                        }]
                    }
                })
                .state('app.wishes', {
                    url: "/wishes",
                    templateUrl: "tpl/wishes.html",
                    controller: 'WishesController',
                    resolve: {
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                    'assets/js/services/hub.js',
                                    'assets/js/controllers/auth.js',
                                    'assets/js/directives/tooltip.js',
                                ], {
                                    insertBefore: '#lazyload_placeholder'
                                })
                                .then(function () {
                                    return $ocLazyLoad.load([
                                        'assets/js/controllers/wishes.js'
                                    ]);
                                });
                        }]
                    }
                })
                .state('app.folders', {
                    url: "/folders",
                    templateUrl: "tpl/folders.html",
                    controller: 'FoldersController',
                    resolve: {
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                    'assets/js/services/hub.js',
                                    'assets/js/controllers/auth.js',
                                ], {
                                    insertBefore: '#lazyload_placeholder'
                                })
                                .then(function () {
                                    return $ocLazyLoad.load([
                                        'assets/js/controllers/folders.js'
                                    ]);
                                });
                        }]
                    }
                })
                .state('app.files', {
                    url: "/files?page",
                    templateUrl: "tpl/files.html",
                    controller: 'FilesController',
                    resolve: {
                        page: ['$stateParams', function ($stateParams) {
                            return $stateParams.page;
                        }],
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                    'assets/js/services/hub.js',
                                    'assets/js/controllers/auth.js',
                                ], {
                                    insertBefore: '#lazyload_placeholder'
                                })
                                .then(function () {
                                    return $ocLazyLoad.load([
                                        'assets/js/controllers/files.js'
                                    ]);
                                });
                        }]
                    }
                })
                .state('app.files_downloads', {
                    url: "/files/downloads",
                    templateUrl: "tpl/files_downloads.html",
                    controller: 'FilesDownloadsController',
                    resolve: {
                        seriesId: ['$stateParams', function ($stateParams) {
                            return $stateParams.seriesId;
                        }],
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                    'assets/js/services/hub.js',
                                    'assets/js/controllers/auth.js',
                                ], {
                                    insertBefore: '#lazyload_placeholder'
                                })
                                .then(function () {
                                    return $ocLazyLoad.load([
                                        'assets/js/controllers/files_downloads.js',
                                    ]);
                                });
                        }]
                    }
                })
                .state('app.logbooks', {
                    url: "/logbooks",
                    templateUrl: "tpl/logbooks.html",
                    controller: 'LogbooksController',
                    resolve: {
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                    'assets/js/services/hub.js',
                                    'assets/js/controllers/auth.js',
                                ], {
                                    insertBefore: '#lazyload_placeholder'
                                })
                                .then(function () {
                                    return $ocLazyLoad.load([
                                        'assets/js/controllers/logbooks.js'
                                    ]);
                                });
                        }]
                    }
                })
                .state('app.transmission', {
                    url: "/transmission",
                    templateUrl: "tpl/transmission.html",
                    controller: 'TransmissionController',
                    resolve: {
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                    'assets/js/services/hub.js',
                                    'assets/js/controllers/auth.js',
                                ], {
                                    insertBefore: '#lazyload_placeholder'
                                })
                                .then(function () {
                                    return $ocLazyLoad.load([
                                        'assets/js/controllers/transmission.js'
                                    ]);
                                });
                        }]
                    }
                })
                .state('app.feeds', {
                    url: "/feeds",
                    templateUrl: "tpl/feeds.html",
                    controller: 'FeedsController',
                    resolve: {
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                    'assets/js/services/hub.js',
                                    'assets/js/controllers/auth.js',
                                ], {
                                    insertBefore: '#lazyload_placeholder'
                                })
                                .then(function () {
                                    return $ocLazyLoad.load([
                                        'assets/js/controllers/feeds.js'
                                    ]);
                                });
                        }]
                    }
                })
                .state('app.feed_details', {
                    url: "/feeds/:feedId?page",
                    templateUrl: "tpl/feed_details.html",
                    controller: 'FeedDetailsController',
                    resolve: {
                        feedId: ['$stateParams', function ($stateParams) {
                            return $stateParams.feedId;
                        }],
                        page: ['$stateParams', function ($stateParams) {
                            return $stateParams.page;
                        }],
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                    'assets/js/services/hub.js',
                                    'assets/js/controllers/auth.js',
                                ], {
                                    insertBefore: '#lazyload_placeholder'
                                })
                                .then(function () {
                                    return $ocLazyLoad.load([
                                        'assets/js/controllers/feed_details.js'
                                    ]);
                                });
                        }]
                    }
                })
                .state('login', {
                    url: "/login",
                    templateUrl: "tpl/login.html",
                    controller: 'AuthController as auth',
                    resolve: {
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                    'assets/js/services/hub.js',
                                ], {
                                    insertBefore: '#lazyload_placeholder'
                                })
                                .then(function () {
                                    return $ocLazyLoad.load([
                                        'assets/js/controllers/auth.js'
                                    ]);
                                });
                        }]
                    }
                })
                .state('app.search', {
                    url: "/search?query",
                    templateUrl: "tpl/search_results.html",
                    controller: 'SearchResultsController',
                    resolve: {
                        query: ['$stateParams', function ($stateParams) {
                            return $stateParams.query;
                        }],
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                    'assets/js/services/hub.js',
                                    'assets/js/controllers/auth.js',
                                    'assets/js/directives/ddTextCollapse.js',
                                    'assets/js/directives/tooltip.js',
                                ], {
                                    insertBefore: '#lazyload_placeholder'
                                })
                                .then(function () {
                                    return $ocLazyLoad.load([
                                        'assets/js/controllers/search_results.js'
                                    ]);
                                });
                        }]
                    }
                });
        }
    ]);