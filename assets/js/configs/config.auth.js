'use strict';

angular.module('app')
    .config(function ($stateProvider, $urlRouterProvider, $authProvider, $httpProvider, $provide, HUB_CONFIG) {
        $httpProvider.interceptors.push(function ($q) {
            return {
                'response': function (response) {
                    if (response.headers().authorization) {
                        var newToken = response.headers().authorization.substring(7);
                        localStorage.setItem('satellizer_token', newToken);
                    }

                    return response;
                }
            }
        });

        $httpProvider.interceptors.push(function ($q, $injector) {
            return {
                responseError: function (rejection) {
                    var $state = $injector.get('$state');
                    var rejectionReasons = ['token_not_provided', 'token_expired', 'token_absent', 'token_invalid'];

                    angular.forEach(rejectionReasons, function (value, key) {
                        if (rejection.data !== undefined) {
                            if (rejection.data.error === value) {
                                localStorage.removeItem('user');
                                localStorage.removeItem('satellizer_token');

                                $state.go('login');
                            }
                        }
                    });

                    return $q.reject(rejection);
                }
            }
        });

        $authProvider.loginUrl = HUB_CONFIG.AUTH_ENDPOINT;
    })
    .run(function ($rootScope, $state) {
        $rootScope.$on('$stateChangeStart', function (event, toState) {
            var user = JSON.parse(localStorage.getItem('user'));

            if (user) {
                $rootScope.authenticated = true;
                $rootScope.currentUser = user;

                if (toState.name === "login") {
                    event.preventDefault();
                    $state.go('app.dashboard');
                }
            }
        });
    });