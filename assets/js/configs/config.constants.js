'use strict';

angular.module('app')
    .constant('HUB_CONFIG', (function () {
        var baseUrl = 'https://api.hub.app';

        return {
            BASE_URL: baseUrl,
            API_URL: baseUrl + '/v1',
            AUTH_ENDPOINT: baseUrl + '/auth'
        }
    })());
